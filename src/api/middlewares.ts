import {
    MEDUSA_RESOLVER_KEYS,
    MedusaAuthenticatedRequest,
    MedusaMiddleware,
    Utils as MedusaUtils,
    Middleware,
  } from "medusa-extender"
  import { NextFunction, Request, Response } from "express"
  
  const multer = require("multer")
  
  @Middleware({
    requireAuth: false,
    routes: [{ method: "post", path: "/route/offer/create" }],
  })
  
  export default class uploadFilesMiddleware implements MedusaMiddleware {
    public consume(
      req: MedusaAuthenticatedRequest | Request,
      res: Response,
      next: NextFunction
    ): void | Promise<void> {
      const upload = multer({ dest: "uploads/" })
      upload.array("files")(req, res, next)
    }
  }
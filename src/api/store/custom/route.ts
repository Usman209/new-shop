import { MedusaRequest, MedusaResponse } from "@medusajs/medusa";
import AuthService from "src/services/auth";

export async function GET(
  req: MedusaRequest,
  res: MedusaResponse
): Promise<void> {
  res.sendStatus(200);
}


export const POST = async (
  req: any,
  res: MedusaResponse
) => {


  console.log('from here ===============================');
  
  const {phone,otp}  = req.body
  const authService: AuthService = req.scope.resolve("authService")

  const updatedCustomer = await authService.verifyUser( phone as any,otp as any)
  res.json({ customer: updatedCustomer })
}



// src/
// └── api/
//     └── store/
//         └── customers/
//             └── route.ts

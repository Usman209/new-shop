import { 
    Customer,
    User,
    type MedusaRequest, 
    type MedusaResponse,
  } from "@medusajs/medusa"
import AuthService from "src/services/auth"
import { EntityManager } from "typeorm"
  
  export const GET = async (
    req: MedusaRequest, 
    res: MedusaResponse
  ) => {

      const authService: AuthService = req.scope.resolve("authService")
  const customerNames = await authService.list()
  res.json({ customers: customerNames })


    // const manager: EntityManager = req.scope.resolve("manager")  


    // const customers = await manager.query('SELECT * FROM customer')  


    // //      // Map over the customers and extract only the names
    // // const customerNames = customers.map(customer => ({
    // //     first_name: customer.first_name,
    // //     last_name: customer.last_name
    // //   }))
        
        
    //     return res.json({  
    //       customers: customers 
    //     })  


//     const authService: AuthService = req.scope.resolve(
//       "authService"
//     )
//   console.log('call from here ===============================?');
  
//     res.json({
//       posts: await authService.list,
//     })
  }



  export const POST = async (
    req: MedusaRequest,
    res: MedusaResponse
  ) => {
    const data  = req.body
    const authService: AuthService = req.scope.resolve("authService")

    const updatedCustomer = await authService.registerUser( data as any)
    res.json({ customer: updatedCustomer })
  }
  

 
//   import {   
//   Customer,   
//   type MedusaRequest,   
//   type MedusaResponse,  
// } from "@medusajs/medusa"  
// import { EntityManager } from "typeorm"  

// export const GET = async (  
//   req: MedusaRequest,   
//   res: MedusaResponse  
// ) => {  
//   const manager: EntityManager = req.scope.resolve("manager")  
//   const customerRepo = manager.getRepository(Customer)  
//   const customers = await customerRepo.find()  
  
//   // Map over the customers and extract only the names
//   const customerNames = customers.map(customer => ({
//     first_name: customer.first_name,
//     last_name: customer.last_name
//   }))
  
//   return res.json({  
//     customers: customerNames,  
//   })  
// }  


// import {   
//     type MedusaRequest,   
//     type MedusaResponse,  
//   } from "@medusajs/medusa"  
//   import { EntityManager } from "typeorm"  
  
//   export const GET = async (  
//     req: MedusaRequest,   
//     res: MedusaResponse  
//   ) => {  
//     const manager: EntityManager = req.scope.resolve("manager")  
    
//     // Raw SQL query to get all customers
//     const customers = await manager.query('SELECT * FROM customers')  
    
//     // Map over the customers and extract only the names
//     const customerNames = customers.map(customer => ({
//       first_name: customer.first_name,
//       last_name: customer.last_name
//     }))
    
//     return res.json({  
//       customers: customerNames,  
//     })  
//   }  
  
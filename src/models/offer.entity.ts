import {
    BeforeInsert,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    PrimaryColumn,
    UpdateDateColumn,
  } from "typeorm"
  
  import { Entity as MedusaEntity } from "medusa-extender"
  import { ulid } from "ulid"
  
  @MedusaEntity()
  @Entity()
  export class Offer {
    @PrimaryColumn({ type: "varchar" })
    id: string
    
    @Column({ nullable: false })
    name: string
  
    @Column({ nullable: true })
    description: string
  
    @Column({ nullable: true })
    thumbnail: string
  
    @Column({ nullable: true })
    metadata: "jsonb"
  
    @CreateDateColumn()
    created_at: Date
  
    @UpdateDateColumn()
    updated_at: Date
  
    @DeleteDateColumn()
    deleted_at: Date
  
    @BeforeInsert()
    private beforeInsert() {
      if (this.id) return
      const id = ulid()
      this.id = `offer_${id}`
    }
  }



  // step 1 
  

import { Repository as MedusaRepository } from "medusa-extender"
import { EntityRepository, Repository } from "typeorm"
import { Offer } from "../models/offer.entity"

@MedusaRepository()
@EntityRepository(Offer)

export class OfferRepository extends Repository<Offer> {}
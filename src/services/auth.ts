import { Customer, TransactionBaseService, User } from "@medusajs/medusa"
import { EntityManager } from "typeorm"
import { v4 as uuidv4 } from 'uuid';
const SibApi = require('sib-api-v3-sdk');
const SibClient = SibApi.ApiClient.instance;


const apiInstance = new SibApi.TransactionalEmailsApi();
const sendSmtpEmail = new SibApi.SendSmtpEmail();

// Authentication
SibClient.authentications["api-key"].apiKey = "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";




class AuthService extends TransactionBaseService {





  protected customerRepository_: any

  constructor(container) {
    super(container)
    this.customerRepository_ = container.customerRepository
  }

  async list(): Promise<any[]> {
    const postRepo = this.activeManager_.getRepository(
      Customer
    )
    return await postRepo.find()
  }

  async  sendOtp(phone: string, otp: string): Promise<void> {
    console.log(`Sending OTP ${otp} to phone ${phone}`);
  
    // Replace the OTP placeholder with the actual OTP
    const personalizedTemplate = `<p>Your OTP code is: ${otp}</p>`;
  
    // Configure the email
    sendSmtpEmail.sender = { email: 'usmanjamil196@gmail.com' };
    sendSmtpEmail.to = [{ email: phone }];
    sendSmtpEmail.subject = 'OTP';
    sendSmtpEmail.htmlContent = personalizedTemplate;
  
    // Send the email
    try {
      const data = await apiInstance.sendTransacEmail(sendSmtpEmail);
      console.log('Email sent successfully:', data);
    } catch (error) {
      console.error('Error sending email:', error);
    }
  }

async  generateOtp(): Promise<string> {
  // Generate a 6-digit OTP. You can modify this as per your requirements.
  return Math.floor(100000 + Math.random() * 900000).toString();
}

async  registerUser(data: { phone: string }): Promise<any> {
  // Check if user already exists
  const userCheckQuery = `
    SELECT * FROM customer WHERE phone = $1;
  `;
  const existingUsers = await this.manager_.query(userCheckQuery, [data.phone]);


  // this test 

  if (existingUsers.length > 0) {
    const user = existingUsers[0];
    const metadata = user.metadata || '{}';

    if (metadata.verified) {
      return { message: 'User is already registered and verified.' };
        } else {
      // User is registered but not verified, generate new OTP and update metadata
      const otp = await this.generateOtp();
      metadata.otp = otp;

      const updateQuery = `
        UPDATE customer SET metadata = $1 WHERE phone = $2 RETURNING *;
      `;
      const updatedUser = await this.manager_.query(updateQuery, [JSON.stringify(metadata), data.phone]);
      await this.sendOtp(data.phone, otp);
      return updatedUser[0];
    }
  } else {
    // Register new user
    const id = uuidv4();
    const otp = await this.generateOtp();
    const metadata = JSON.stringify({ otp: otp });

    const insertQuery = `
      INSERT INTO customer (id, phone, metadata)
      VALUES ($1, $2, $3)
      RETURNING *;
    `;
    const newUser = await this.manager_.query(insertQuery, [id, data.phone, metadata]);
    await this.sendOtp(data.phone, otp);
    return newUser[0];
  }
}

async  verifyUser(phone: string, otp: string): Promise<any> {
  // Check if user exists
  const userCheckQuery = `
    SELECT * FROM customer WHERE phone = $1;
  `;
  const existingUsers = await this.manager_.query(userCheckQuery, [phone]);

  if (existingUsers.length === 0) {
    throw new Error('User not found.');
  }

  const user = existingUsers[0];


  // const metadata = JSON.parse(user.metadata || '{}');

  // console.log('here 123pk===============',metadata);
  

  if (user.metadata.otp === otp) {
    // OTP is correct, update metadata to set verified flag
    user.metadata.verified = true;

    const updateQuery = `
      UPDATE customer SET metadata = $1 WHERE phone = $2 RETURNING *;
    `;
    const updatedUser = await this.manager_.query(updateQuery, [JSON.stringify(user.metadata), phone]);
    return updatedUser[0];
  } else {
    throw new Error('Invalid OTP.');
  }
}
  

  }


 


export default AuthService  // Export AuthService

  


 // protected customerRepository_: any

  // constructor(container) {{
  //   super(container)
  //   this.customerRepository_ = container.customerRepository
  // }}

  // async list(): Promise<any[]> {{
  //   console.log('from service ==============================');
  //   const rawQuery = 'SELECT first_name, last_name FROM customer';
  //   const customers = await this.manager_.query(rawQuery);

  //   return customers
  // }}





// // In your service
// class AuthService extends TransactionBaseService {
//   async list(): Promise<any[]> {
//     const customerRepo = this.activeManager_.withRepository()
//     const customers = await customerRepo.find()
//     const customerNames = customers.map(customer => ({
//       first_name: customer.first_name,
//       last_name: customer.last_name
//     }))
//     return customerNames
//   }
// }

// // In your route
// app.get('/customers', async (req, res) => {
//   const authService: AuthService = req.scope.resolve("authService")
//   const customerNames = await authService.list()
//   res.json({ customers: customerNames })
// })



//   import type {   
//     MedusaRequest,   
//     MedusaResponse,  
//   } from "@medusajs/medusa"  
//   import { Customer } from "../models/customer"  
//   import { EntityManager } from "typeorm"  
    
//   export const GET = async (  
//     req: MedusaRequest,   
//     res: MedusaResponse  
//   ) => {  
//     const manager: EntityManager = req.scope.resolve("manager")  
//     const customerRepo = manager.getRepository(Customer)  
    
//     return res.json({  
//       customers: await customerRepo.find(),  
//     })  
//   }  
  


// import type {   
//     MedusaRequest,   
//     MedusaResponse,  
//   } from "@medusajs/medusa"  
//   import { EntityManager } from "typeorm"  
    
//   export const GET = async (  
//     req: MedusaRequest,   
//     res: MedusaResponse  
//   ) => {  
//     const customerRepository = req.scope.resolve("customerRepository")  
//     const manager: EntityManager = req.scope.resolve("manager")  
//     const customerRepo = manager.withRepository(customerRepository)  
    
//     return res.json({  
//       customers: await customerRepo.find(),  
//     })  
//   }  
  



// class AuthService extends TransactionBaseService {
//   protected customerRepository_: any

//   constructor(container) {{
//     super(container)
//     this.customerRepository_ = container.customerRepository
//   }}

//   async list(): Promise<any[]> {{
//     console.log('from service ==============================');
//     const rawQuery = 'SELECT first_name, last_name FROM customer';
//     const customers = await this.manager_.query(rawQuery);
//     const customerNames = customers.map(customer => ({{
//       first_name: customer.first_name,
//       last_name: customer.last_name
//     }}))
//     return customerNames
//   }}
// }



  // async create(data: { first_name: string, last_name: string, email: string, password: string }): Promise<any> {
  //     const customerRepo =this.activeManager_.getRepository(
  //       Customer
  //     )
  //     const customer = customerRepo.create(data)
  //     const result = await customerRepo.save(customer)
  //     return result
    
  // }


  // async create(data: { first_name: string, last_name: string, email: string, password_hash: string }): Promise<any> {
  //   const id = uuidv4();
  //   const rawQuery = `
  //     INSERT INTO customer (id, first_name, last_name, email, password_hash)
  //     VALUES ('${id}', '${data.first_name}', '${data.last_name}', '${data.email}', '${data.password_hash}')
  //     RETURNING *;
  //   `;
  //   const result = await this.manager_.query(rawQuery);
  //   return result;
  // }